package jobs;

import java.util.Arrays;
import java.util.List;

import play.Logger;
import play.jobs.Every;
import play.jobs.Job;
import play.jobs.OnApplicationStart;
import services.MySqlGeneralLogMonitor;
import services.MySqlLogMonitor;
import services.PapertrailLogger;

@OnApplicationStart
@Every("1s")
public class MySqlMonitorJob extends Job {

    private final List<MySqlLogMonitor> logMonitors = Arrays.<MySqlLogMonitor>asList(
        new MySqlGeneralLogMonitor()
    );

    @Override
    public void doJob() throws Exception {
        try {
            logNewMySqlLogs();

        } catch (Exception e) {
            Logger.error(e, e.getLocalizedMessage());
        }
    }

    private void logNewMySqlLogs() {
        for (MySqlLogMonitor logMonitor : logMonitors) {
            List<String> logs = logMonitor.getNewLogs();
            for (String log : logs) {
                PapertrailLogger.log(log);
            }
        }
    }

}

package services;

import java.util.List;

public interface MySqlLogMonitor {

    List<String> getNewLogs();

}

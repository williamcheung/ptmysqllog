package services;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import models.MySqlGeneralLog;

public class MySqlGeneralLogMonitor implements MySqlLogMonitor {

    private Date lastTime = new Date(); // assume clock synchronized with MySql server

    @Override
    public List<String> getNewLogs() {
        List<MySqlGeneralLog> logs = MySqlGeneralLog.find(
            "select gl from MySqlGeneralLog gl where eventTime > ? order by eventTime", lastTime).fetch();
        // Note: Granularity is second, so may miss events in the same second on
        // the next poll. Decided to accept this possible loss of information
        // in favour of avoiding duplicating logs within the same second as last
        // time on each new poll. This decision should be reconsidered if the
        // frequency of events within the same second is high.
        List<String> messages = new ArrayList<String>(logs.size());

        for (MySqlGeneralLog log : logs) {
            if (matchQueriesToIgnore(log)) {
                continue; // skip queries which cause noise
            }
            String message = MessageFormat.format("{0,date,yyyy-MM-dd HH:mm:ss} {1} {2} {3} {4} {5}",
                log.eventTime,
                log.userHost,
                log.threadId,
                log.serverId,
                log.commandType,
                log.argument);
            messages.add(message);

            lastTime = log.eventTime;
        }

        return messages;
    }

    private static boolean matchQueriesToIgnore(MySqlGeneralLog log) {
        return
            // following from RDS stats polling every 5 minutes
            log.userHost.toLowerCase().contains("[rdsadmin]".toLowerCase()) ||
            (log.commandType.equalsIgnoreCase("Query") && (
            // following from self via Hibernate
            log.argument.equalsIgnoreCase("SHOW FULL TABLES FROM `mysql` LIKE 'PROBABLYNOT'") ||
            log.argument.toLowerCase().endsWith("order by mysqlgener0_.event_time".toLowerCase()) ||
            // following may not be by self, but probably, so match to avoid noise
            log.argument.toLowerCase().endsWith("commit".toLowerCase())) ||
            log.argument.toLowerCase().endsWith("SET autocommit=0".toLowerCase()) ||
            log.argument.toLowerCase().endsWith("SET autocommit=1".toLowerCase()) ||
            // following from RDS heartbeat polling every 15 seconds
            log.argument.toLowerCase().contains("rds_heartbeat2".toLowerCase()) ||
            // following probably from RDS heartbeat polling every 15 seconds
            log.argument.toLowerCase().endsWith("SELECT 1".toLowerCase()));
    }

}

package services;

import org.apache.log4j.Logger;

public class PapertrailLogger {

    private static final String PAPERTRAIL_LOGGER_NAME = "papertrail"; // defined in conf/log4j.xml

    private static final Logger LOGGER  = org.apache.log4j.Logger.getLogger(PAPERTRAIL_LOGGER_NAME);

    public static void log(String message) {
        LOGGER.info(message);
    }

}

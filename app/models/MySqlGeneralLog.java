package models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import play.db.jpa.GenericModel;

@Entity
@Table(name = "general_log")
public class MySqlGeneralLog extends GenericModel {

    @Id
    @Column(name = "event_time", nullable = false)
    public Date eventTime;

    @Id
    @Column(name = "user_host", nullable = false)
    public String userHost;

    @Id
    @Column(name = "thread_id", nullable = false)
    public int threadId;

    @Id
    @Column(name = "server_id", nullable = false)
    public int serverId;

    @Id
    @Column(name = "command_type", nullable = false)
    public String commandType;

    @Id
    @Column(name = "argument", nullable = false)
    public String argument;

}
